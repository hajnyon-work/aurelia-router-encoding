# `aurelia-router-encode-params`

Implementation of aurelia's router with decoding/encoding mechanism for route's parameters handling (route-href attribute, activate method, route href generation). See:

-   `./src/router.ts` for router methods override
-   `./src/main.ts` for router settings during aurelia bootstrap
-   params encoding
    -   `./src/users/index.ts` for router's generate method
    -   `./src/users/index.html` for route-href attribute

### Used resources

-   https://kristianmandrup.gitbooks.io/aurelia-app-amazement/content/advanced-routing.html
-   https://aurelia.io/docs/templating/custom-attributes#options-binding
-   https://github.com/aurelia/router/issues/631

---

This project is bootstrapped by [aurelia-cli](https://github.com/aurelia/cli).

For more information, go to https://aurelia.io/docs/cli/webpack

## Run dev app

Run `npm start`, then open `http://localhost:8080`

You can change the standard webpack configurations from CLI easily with something like this: `npm start -- --open --port 8888`. However, it is better to change the respective npm scripts or `webpack.config.js` with these options, as per your need.

To enable Webpack Bundle Analyzer, do `npm run analyze` (production build).

To enable hot module reload, do `npm start -- --hmr`.

To change dev server port, do `npm start -- --port 8888`.

To change dev server host, do `npm start -- --host 127.0.0.1`

**PS:** You could mix all the flags as well, `npm start -- --host 127.0.0.1 --port 7070 --open --hmr`

For long time aurelia-cli user, you can still use `au run` with those arguments like `au run --env prod --open --hmr`. But `au run` now simply executes `npm start` command.

## Build for production

Run `npm run build`, or the old way `au build --env prod`.

## Unit tests

Run `au test` (or `au jest`).

To run in watch mode, `au test --watch` or `au jest --watch`.
