import { Aurelia } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
import { Router } from 'aurelia-router';
import { RouteLoader } from 'aurelia-router';
import { RouteHref, RouterView, TemplatingRouteLoader } from 'aurelia-templating-router';

import environment from '../config/environment.json';

import { CustomRouter } from './router';

export function configure(aurelia: Aurelia): void {
    aurelia.use
        .defaultBindingLanguage()
        .defaultResources()
        .eventAggregator()
        .history()
        .singleton(RouteLoader, TemplatingRouteLoader)
        .singleton(Router, CustomRouter)
        .globalResources([RouterView, RouteHref])
        .feature(PLATFORM.moduleName('resources/index'));
    aurelia.container.registerAlias(Router, CustomRouter);

    aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

    if (environment.testing) {
        aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
    }

    aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
