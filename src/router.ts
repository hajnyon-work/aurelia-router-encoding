import { RecognizedRoute, RouteRecognizer } from 'aurelia-route-recognizer';
import { AppRouter, RouteConfig } from 'aurelia-router';

class CustomRouteRecognizer extends RouteRecognizer {
    public override recognize(path: string): RecognizedRoute[] | void {
        const result: RecognizedRoute[] | void = super.recognize(path);
        if (typeof result === 'object') {
            for (const route of result) {
                for (const param of Object.keys(route.params)) {
                    if (param === 'childRoute') {
                        continue;
                    }
                    route.params[param] = decodeURIComponent(route.params[param]);
                }
            }
            return result;
        }
        return result;
    }
}

export class CustomRouter extends AppRouter {
    protected _recognizer: CustomRouteRecognizer;
    protected _childRecognizer: CustomRouteRecognizer;

    public override generate(nameOrRoute: string | RouteConfig, params?: any, options?: any): string {
        if (params) {
            for (const param of Object.keys(params)) {
                params[param] = encodeURIComponent(params[param]);
            }
        }
        return super.generate(nameOrRoute, params, options);
    }

    public override reset() {
        super.reset();
        this._recognizer = new CustomRouteRecognizer();
        this._childRecognizer = new CustomRouteRecognizer();
    }
}
