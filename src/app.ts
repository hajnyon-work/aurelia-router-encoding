import { PLATFORM } from 'aurelia-framework';
import { NavigationInstruction, Next, Router, RouterConfiguration } from 'aurelia-router';

export class App {
    router: Router;

    configureRouter(config: RouterConfiguration, router: Router): void {
        this.router = router;
        config.title = 'AAA';
        config.options.pushState = true;
        config.options.root = '/';

        config.map([
            { route: ['', 'home'], name: 'home', title: 'Home', nav: true, moduleId: PLATFORM.moduleName('./home/index') },
            { route: 'users', name: 'users', title: 'Users', nav: true, moduleId: PLATFORM.moduleName('./users/index') },
            { route: 'users/:id/detail', name: 'userDetail', moduleId: PLATFORM.moduleName('./users/detail') },
            { route: 'files/*filePath/detail', name: 'files', moduleId: PLATFORM.moduleName('./files/index') }
        ]);
    }
}
