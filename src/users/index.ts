import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@autoinject()
export class Users {
    public constructor(private router: Router) {}
    public activate() {
        console.log(this.router.generate('userDetail', { id: 'aaa/bbb' }));
    }
}
